PROCID=$(ps -ef | grep mongod | grep ^core | grep -v grep | grep -v stopmongo | awk '{print $2}')

for p in $PROCID
do
  kill -9 $p
done
