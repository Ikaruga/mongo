MONGO_DIR = /home/core/mongo
MKDIR = mkdir -p


all : log data.solo data.rep

log : ${MONGO_DIR}/log
data.solo : ${MONGO_DIR}/data/solo
data.rep : ${MONGO_DIR}/data/rep1 ${MONGO_DIR}/data/rep2 ${MONGO_DIR}/data/rep3


${MONGO_DIR}/data/rep1 : 
	${MKDIR} ${MONGO_DIR}/data/rep1

${MONGO_DIR}/data/rep2 : 
	${MKDIR} ${MONGO_DIR}/data/rep2

${MONGO_DIR}/data/rep3 : 
	${MKDIR} ${MONGO_DIR}/data/rep3

${MONGO_DIR}/data/solo : 
	${MKDIR} ${MONGO_DIR}/data/solo

${MONGO_DIR}/log : 
	${MKDIR} ${MONGO_DIR}/log
